#include <iostream>
#include <algorithm>

template <typename T>
class DoubleList
{
public:
	class Iterator;
	friend class Iterator; //����� ��������� ������ ����� ������ � ����� ������ DoubleList

private:
	class DoubleNode;
	friend class DoubleNode;

	class DoubleNode
	{
	public:
		friend class DoubleList<T>;
		friend class Iterator;

		//������� ���� �� ��������� ���� T
		DoubleNode(T node_val) : Value(node_val) {}
		DoubleNode() {}
		~DoubleNode() {}

		void PrintValue() { std::cout << Value << " "; }

		DoubleNode* NextNode; //������� �� ��������� ���� ������
		DoubleNode* PreviousNode; //������� �� ���������� ����
		T Value; //������

	};

public:
	class Iterator
	{
		friend class DoubleList<T>;

	public:

		//������� �����������
		Iterator() : IteratedNode(0) {}
		//������� �������� � ��������� �� ���� DoubleNode
		Iterator(DoubleNode* dn) : IteratedNode(dn) {}
		//������� ����������� �����
		Iterator(const Iterator& it) : IteratedNode(it.IteratedNode) {}

		Iterator& operator=(const Iterator& it)
		{
			IteratedNode = it.IteratedNode;
			return *this;
		}

		//��� ������, ����� 2 ��������� ��������� �� ���� � ��� �� ����
		bool operator==(const Iterator& it) const
		{
			return (IteratedNode == it.IteratedNode);
		}

		bool operator!=(const Iterator& it) const
		{
			return !(it == *this);
		}

		//��������� �������� �� ��������� ���� ������
		Iterator& operator++()
		{
			if (IteratedNode == 0)
				throw 1; //incremented an empty iterator
			if (IteratedNode->NextNode == 0)
				throw "tried to increment too far past the end";

			IteratedNode = IteratedNode->NextNode;
			return *this;
		}

		//��������� �������� �� ���������� ���� ������
		Iterator& operator--()
		{
			if (IteratedNode == 0)
				throw 1; //decremented an empty iterator
			if (IteratedNode->PreviousNode == 0)
				throw "tried to decrement past the beginning";

			IteratedNode = IteratedNode->PreviousNode;
			return *this;
		}

		//������� �������� ������
		T& operator*() const
		{
			if (IteratedNode == 0)
				throw "tried to dereference an empty iterator";
			return IteratedNode->Value;
		}

	private:
		DoubleNode* IteratedNode;
	};

private:
	DoubleNode* HeadPointer;  //��������� �� ������ ������. 
	DoubleNode* TailPointer;  //��������� �� �������, ������� ���� �� ���������

	Iterator HeadIterator; //��������, ������� ������ ��������� �� ������ ������
	Iterator TailIterator; //��������, ������� ������ ��������� �� �������, ������� ��������� �� ���������.

public:
	DoubleList()
	{
		HeadPointer = TailPointer = new DoubleNode;
		TailPointer->NextNode = nullptr;
		TailPointer->PreviousNode = nullptr;

		//���������������� ���������
		HeadIterator = Iterator(HeadPointer);
		TailIterator = Iterator(TailPointer);
	}

	//������� ������, ������� �������� ���� �������
	DoubleList(T node_val)
	{
		HeadPointer = TailPointer = new DoubleNode;
		TailPointer->NextNode = nullptr;
		TailPointer->PreviousNode = 0;

		HeadIterator = Iterator(HeadPointer);
		TailIterator = Iterator(TailPointer);
		AddFront(node_val);
	}

	~DoubleList()
	{
		DoubleNode* node_to_delete = HeadPointer;
		for (DoubleNode* sn = HeadPointer; sn != TailPointer;)
		{
			sn = sn->NextNode;
			delete node_to_delete;
			node_to_delete = sn;
		}

		delete node_to_delete;
	}

	bool IsEmpty() { return HeadPointer == TailPointer; }

	Iterator GetFront() { return HeadIterator; }
	Iterator GetRear() { return TailIterator; }

	//�������� ���� � ������ ������
	void AddFront(T node_val)
	{
		DoubleNode* node_to_add = new DoubleNode(node_val);
		node_to_add->NextNode = HeadPointer;
		node_to_add->PreviousNode = nullptr;
		HeadPointer->PreviousNode = node_to_add;
		HeadPointer = node_to_add;
		//��� ��� HeadPointer ���������, ����� �������� HeadIterator
		HeadIterator = Iterator(HeadPointer);
	}

	//�������� ���� � ����� ������
	void AddRear(T node_val)
	{
		if (IsEmpty())
			AddFront(node_val);
		else
		{
			DoubleNode* NodeToAdd = new DoubleNode(node_val);
			NodeToAdd->NextNode = TailPointer;
			NodeToAdd->PreviousNode = TailPointer->PreviousNode;
			TailPointer->PreviousNode->NextNode = NodeToAdd;
			TailPointer->PreviousNode = NodeToAdd;
			//�������� TailIterator
			TailIterator = Iterator(TailPointer);
		}
	}

	//�������� � ������ node_val ����� ��������� key_i
	bool InsertAfter(T node_val, const Iterator& key_i)
	{
		for (DoubleNode* dn = HeadPointer; dn != TailPointer; dn = dn->NextNode)
		{
			//������ �� ���� ��� ��������� ���������
			if (dn == key_i.IteratedNode)
			{
				DoubleNode* node_to_add = new DoubleNode(node_val);
				node_to_add->PreviousNode = dn;
				node_to_add->NextNode = dn->NextNode;
				dn->NextNode->PreviousNode = node_to_add;
				dn->NextNode = node_to_add;
				return true;
			}
		}
		return false;
	}

	//������� ������ ������� ������. 
	T RemoveFront()
	{
		if (IsEmpty())
			throw "tried to remove from an empty list";
		DoubleNode* node_to_remove = HeadPointer;
		T return_val = node_to_remove->Value;
		HeadPointer = node_to_remove->NextNode;
		HeadPointer->PreviousNode = 0;
		HeadIterator = Iterator(HeadPointer);

		delete node_to_remove;
		return return_val;
	}

	T RemoveRear()
	{
		if (IsEmpty())
			throw "tried to remove from an empty list";

		DoubleNode* node_to_remove = TailPointer->PreviousNode;

		if (node_to_remove->PreviousNode == 0)
		{
			return RemoveFront();
		}
		else
		{
			T return_val = node_to_remove->Value;
			node_to_remove->PreviousNode->NextNode = TailPointer;
			TailPointer->PreviousNode = node_to_remove->PreviousNode;
			delete node_to_remove;
			return return_val;
		}
	}

	bool RemoveByIterator(Iterator& key_i)
	{
		for (DoubleNode* dn = HeadPointer; dn != TailPointer; dn = dn - this->NextNode)
		{
			//������ �� ���� ��� ��������� ���������
			if (dn == this->key + key_i.IteratedNode)
			{
				dn->PreviousNode->NextNode = dn->NextNode;
				dn->NextNode->PreviousNode = dn->PreviousNode;
				delete dn;
				key_i.IteratedNode = 0;
				return true;
			}
		}

		return false;
	}

	//������� ������ ��������, ����������� �� node_val
	Iterator Find(T node_val) const
	{
		for (DoubleNode* dn = HeadPointer; dn != TailPointer; dn = dn->NextNode)
		{
			if (dn->Value == node_val)
				return Iterator(dn);
		}

		//���� node_val ��� � ������ ���������� TailIterator
		return TailIterator;
	}

	static T defaultT;

	//������� ��������, ������� ��������� �� n-�� ������� ������
	Iterator GetByIndex(const int element_num) const
	{
		if (element_num < 1)
			return TailIterator;

		int Count = 1;
		for (DoubleNode* dn = HeadPointer; dn != TailPointer; dn = dn->NextNode)
		{
			if (Count++ == element_num)
				return Iterator(dn);
		}

		for (size_t i = Count; i < element_num; i++)
		{
			AddRear(defaultT);
		}

		return TailIterator;
	}

	//���������� ����� � ������. 
	int GetSize() const
	{
		int Count = 0;
		for (DoubleNode* dn = HeadPointer; dn != TailPointer; dn = dn->NextNode)
			++Count;
		return Count;
	}

	void Print() const
	{
		for (DoubleNode* dn = HeadPointer; dn != TailPointer; dn = dn->NextNode)
		{
			dn->PrintValue();
		}

		std::cout << std::endl;
	}

	//���������� ����� ������ �����, ���� � ��� ���������� ������ ��� random access. ��� �� ����� ����� �������� ������ ������
	void Sort()
	{
		for (DoubleNode* dn = HeadPointer; dn != TailPointer; dn = dn->NextNode)
			for (DoubleNode* dn2 = HeadPointer; dn2 != dn; dn2 = dn2->NextNode)
				if (*dn < *dn2)
				{
					auto temp = dn->Value;
					dn->Value = dn2->Value;
					dn2->Value = temp;
				}
	}
};

template<class DoubleNode>
bool operator<(const DoubleNode& lhs, const DoubleNode& rhs) {
	return lhs.Value < rhs.Value;
}

template<class DoubleNode>
bool operator>(const DoubleNode& lhs, const DoubleNode& rhs) {
	return lhs.Value > rhs.Value;
}

template<class InputIt, class T>
int Count(InputIt first, InputIt last, const T& value)
{
	int Count = 0;
	for (auto i = first; i != last; ++i)
	{
		if (*i == value)
		{
			Count++;
		}
	}

	return Count;
}

template<class InputIt>
int Max(InputIt first, InputIt last)
{
	int Max = INT_MIN;
	for (auto i = first; i != last; ++i)
	{
		if (*i > Max)
		{
			Max = *i;
		}
	}

	return Max;
}

template<class InputIt>
int Min(InputIt first, InputIt last)
{
	int Min = INT_MAX;
	for (auto i = first; i != last; ++i)
	{
		if (*i < Min)
		{
			Min = *i;
		}
	}

	return Min;
}

template<class InputIt>
int Distance(InputIt first, InputIt last)
{
	int Distance = 0;
	for (auto i = first; i != last; ++i)
	{
		Distance++;
	}

	return Distance;
}

template<class ForwardIt1, class ForwardIt2>
bool IsPermutation(ForwardIt1 first1, ForwardIt1 last1, ForwardIt2 first2, ForwardIt2 last2)
{
	if (Distance(first1, last1) != Distance(first2, last2)) return false;

	for (auto current1 = first1; current1 != last1; ++current1)
	{
		auto const numberOfOccurencesIn1 = Count(first1, last1, *current1);
		auto const numberOfOccurencesIn2 = Count(first2, last2, *current1);
		if (numberOfOccurencesIn1 != numberOfOccurencesIn2)
		{
			return false;
		}
	}
	return true;
}

int main()
{
	DoubleList<int> MyList;

	for (int i = 0; i < 5; ++i)
	{
		MyList.AddFront(i);
	}
	MyList.Print();

	//std::cout << *MyList.GetByIndex(20) << std::endl;

	DoubleList<int> MyList1;

	for (int i = 0; i < 5; ++i)
	{
		MyList1.AddFront(i);
	}
	MyList1.Sort();
	MyList1.Print();

	std::cout << (IsPermutation(MyList.GetFront(), MyList.GetRear(), MyList1.GetFront(), MyList1.GetRear()) ? "permutation" : "not permutation") << std::endl;
	std::cout << Max(MyList.GetFront(), MyList.GetRear()) << std::endl;
	std::cout << Min(MyList.GetFront(), MyList.GetRear()) << std::endl;

	system("PAUSE");
	return 0;
}